<?php

namespace App\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class HomeAction
{
    //                  Classic
//    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
//    {
//        $response->getBody()->write('Hello Home!');
//        return $response;
//    }

    //                  JSON
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $response->getBody()->write(json_encode(['Hello' => 'Home!']));
        return $response->withHeader('Content-Type', 'application/json');

    }
}