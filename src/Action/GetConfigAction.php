<?php

namespace App\Action;

use DI\ContainerBuilder;
use DI\DependencyException;
use DI\NotFoundException;
use Exception;
use Laminas\Config\Config;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GetConfigAction
{
    /**
     * @throws DependencyException
     * @throws NotFoundException
     * @throws Exception
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        //recreation du container
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions(__DIR__ . '/../../config/container.php');
        $container = $containerBuilder->build();

        // get config via DI
        $confing = $container->get(Config::class);
        //to array pour avoir le tableau de config original, l'objet en lui meme est difficile à afficher
        $response->getBody()->write(json_encode($confing->toArray()));
        //$response->getBody()->write(json_encode($confing->get('db')->toArray()));
        return $response->withHeader('Content-Type', 'application/json');
    }
}