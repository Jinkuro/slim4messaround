<?php

namespace App\Action;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ExempleAction
{
    private MyService $myService;
    private JsonRenderer $jsonRender;

    public function __construct(MyService $myService, JsonRenderer $jsonRender)
    {
        $this->myService = $myService;
        $this->jsonRender = $jsonRender;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        //1 exemple for collecting data from the http request
        $data = (array)$request->getParsedBody();

        //2 Invoke the Domain Service with data
        // (if required) and retain the result
        $domainResult = $this->myService->doSomething($data);

        //3 Build and return hhtp response
        return $this->jsonRender->json($response, $domainResult);
    }
}