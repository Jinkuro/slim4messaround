<?php
/**
 * This file is required to set up the global middlewares
 */

use Slim\App;

return function (App $app) {
    //middleware for parsing json, form data and xml
    $app->addBodyParsingMiddleware();

    //built in routing middleware
    $app->addRoutingMiddleware();

    //middleware catching exceptions and errors
    $app->addErrorMiddleware(true, true, true);
};