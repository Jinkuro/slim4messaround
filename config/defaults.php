<?php
// Turn off error reposrting for security reasons
error_reporting(0);
ini_set('display_errors', '0');
ini_set('display_startup_errors', '0');

$settings = [];

//Detect Environnement => test? => prod? so dev
$settings['env'] = $_ENV['APP_ENV'] ?? $_SERVER['APP_ENV'] ?? 'dev';

return $settings;