<?php

//php unit test environement

use Psr\Log\NullLogger;

return function (array $settings): array {
    $settings['error']['display_error_details'] = true;
    $settings['error']['log_errors'] = true;

// Test Database Settings
// $settings['db']['database'] = 'my_test_db';

// Mocked Logger settings
    $settings['logger'] = [
        'path' => '',
        'level' => 0,
        'test' => new NullLogger(),
    ];
    return $settings;
};