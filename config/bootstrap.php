<?php

use DI\ContainerBuilder;
use Slim\App;

require_once __DIR__.'/../vendor/autoload.php';

$containerBuilder = new ContainerBuilder();
//ajoute la definition au containerBuilder
$containerBuilder->addDefinitions(__DIR__ . "/container.php");

//créer une instance du conteneur parametré
$container = $containerBuilder->build();

//créer une instance Slim App
$app = $container->get(App::class);

// => (require xxx.php)($app) => execute le fichier php définit du coté require en passe en argument $app
//Enregistre les routes (définie ?)
(require __DIR__ . '/routes.php')($app);

//Enregistre les middlewares
(require __DIR__ . '/middleware.php')($app);

return $app;