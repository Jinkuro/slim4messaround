<?php

// Load the default settings
$settings = require __DIR__ . '/defaults.php';

// Overwrite default settings with environnement specific local settings
/**
 * /{local.%s,env,../../env}.php: This is the pattern part of the string. It includes different filenames enclosed in curly braces, separated by commas. The filenames are:
 * local.%s: This represents a local configuration file specific to the current environment ($settings['env'] value). The %s placeholder will be replaced with the environment value.
 * env: This represents a general configuration file named env.php.
 * ../../env: This represents a relative path to a general configuration file named env.php, located two directories above the current file.
 */
$configFiles = sprintf('%s/{local.%s,env,../../env}.php', __DIR__, $settings['env']);

foreach (glob($configFiles, GLOB_BRACE) as $file) {
    $local = require $file;
    if(is_callable($local)){
        $settings = $local($settings);
    }
}
return $settings;