<?php

use DI\ContainerBuilder;
use Slim\App;

return function (App $app) {
    $app->get('/', \App\Action\HomeAction::class);

    $app->get('/testconfig', \App\Action\GetConfigAction::class);
};