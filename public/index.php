<?php
/**
 * For security reasons you should always place your front-controller (index.php) into the public/ directory.
 * You should never place the front controller directly into the project root directory
 */

/**
 * Be careful: The public/ directory is only the DoumentRoot of your webserver, but it’s never part of your
 * base path and the official url.
 * Good URLs:
 * https://www.example.com
 * https://www.example.com/users
 * httsp://www.example.com/my-app
 * https://www.example.com/my-app/users
 * Bad URLs:
 * https://www.example.com/public
 * https://www.example.com/public/users
 * https://www.example.com/public/index.php
 * https://www.example.com/my-app/public
 * https://www.example.com/my-app/public/users
 */

(require __DIR__ . '/../config/bootstrap.php')->run();